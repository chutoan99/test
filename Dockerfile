# Dockerfile for root
FROM node:18-alpine

WORKDIR /app

COPY package.json package-lock.json ./
COPY . .

RUN npm install --f

EXPOSE 8080
CMD ["npm", "run", "dev"]