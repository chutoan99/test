
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export interface GetPostDto {
    pageNumber: number;
    pageSize: number;
    orderBy: string;
    direction: string;
    title: string;
    start: string;
    address: string;
    categoryCode: string;
    provinceCode: string;
    priceNumber?: Nullable<number[]>;
    areaNumber?: Nullable<number[]>;
}

export interface UpdateUserDto {
    name: string;
    avatar?: Nullable<string>;
    phone?: Nullable<string>;
    zalo?: Nullable<string>;
    file?: Nullable<string>;
}

export interface PostCreateDto {
    userid: string;
    address: string;
    areaCode: string;
    areaNumber: number;
    categoryCode: string;
    description: string;
    images: string[];
    label: string;
    type: string;
    priceCode: string;
    priceNumber: number;
    province: string;
    target: string;
    title: string;
    start: number;
}

export interface PostUpdateDto {
    address: string;
    title: string;
}

export interface LoginDto {
    phone: string;
    password: string;
}

export interface RegisterDto {
    name: string;
    phone: string;
    password: string;
}

export interface AreaSchema {
    id: string;
    order: number;
    value: string;
    createdAt: DateTime;
    updatedAt: DateTime;
}

export interface AreaResponse {
    err: number;
    msg: string;
    response: AreaSchema[];
}

export interface AttributeSchema {
    id: string;
    price: string;
    acreage: string;
    published: string;
    hashtag: string;
    createdAt: DateTime;
    updatedAt: DateTime;
}

export interface AttributeResponse {
    err: number;
    msg: string;
    response: AttributeSchema[];
}

export interface OverviewSchema {
    id: string;
    code: string;
    area: string;
    type: string;
    target: string;
    created: string;
    expired: string;
    bonus: string;
    createdAt: DateTime;
    updatedAt: DateTime;
}

export interface PostSchema {
    id?: Nullable<string>;
    title?: Nullable<string>;
    start?: Nullable<string>;
    labelCode?: Nullable<string>;
    address?: Nullable<string>;
    attributesId?: Nullable<string>;
    categoryCode?: Nullable<string>;
    priceCode?: Nullable<string>;
    areaCode?: Nullable<string>;
    provinceCode?: Nullable<string>;
    description?: Nullable<string>;
    userId?: Nullable<string>;
    overviewId?: Nullable<string>;
    imagesId?: Nullable<string>;
    priceNumber?: Nullable<number>;
    areaNumber?: Nullable<number>;
    images?: Nullable<string>;
    postImg?: Nullable<string>;
    total: number;
    createdAt?: Nullable<DateTime>;
    updatedAt?: Nullable<DateTime>;
    user: UserSchema;
    attributes: AttributeSchema;
    overviews: OverviewSchema;
}

export interface UserSchema {
    id: string;
    name: string;
    password: string;
    avatar?: Nullable<string>;
    phone?: Nullable<string>;
    zalo?: Nullable<string>;
    file?: Nullable<string>;
    post?: Nullable<PostSchema[]>;
    createdAt: DateTime;
    updatedAt: DateTime;
}

export interface LoginSchema {
    err: number;
    msg: string;
    token?: Nullable<string>;
    response: UserSchema;
}

export interface RegisterSchema {
    err: number;
    msg: string;
    token?: Nullable<string>;
}

export interface CategorySchema {
    id: string;
    value: string;
    header: string;
    subHeader: string;
    path: string;
    createdAt: DateTime;
    updatedAt: DateTime;
}

export interface CategoryResponse {
    err: number;
    msg: string;
    response: CategorySchema[];
}

export interface PostCreateResponse {
    err: number;
    msg: string;
}

export interface PostDeleteResponse {
    err: number;
    msg: string;
}

export interface PostIdResponse {
    err: number;
    msg: string;
    response: PostSchema;
}

export interface PostUpdateResponse {
    err: number;
    msg: string;
    response: PostSchema;
}

export interface PostResponse {
    err: number;
    msg: string;
    totalPage: number;
    total: number;
    pageNumber: number;
    pageSize: number;
    response: PostSchema[];
}

export interface PriceSchema {
    id: string;
    order: number;
    value: string;
    createdAt: DateTime;
    updatedAt: DateTime;
}

export interface PriceResponse {
    err: number;
    msg: string;
    response: PriceSchema[];
}

export interface ProvinceSchema {
    id: string;
    value: string;
    createdAt: DateTime;
    updatedAt: DateTime;
}

export interface ProvinceResponse {
    err: number;
    msg: string;
    response: ProvinceSchema[];
}

export interface UserIdResponse {
    err: number;
    msg: string;
    response: UserSchema;
}

export interface UserUpdateResponse {
    err: number;
    msg: string;
}

export interface UserResponse {
    err: number;
    msg: string;
    response: UserSchema[];
}

export interface IQuery {
    area(): AreaResponse | Promise<AreaResponse>;
    category(): CategoryResponse | Promise<CategoryResponse>;
    price(): PriceResponse | Promise<PriceResponse>;
    province(): ProvinceResponse | Promise<ProvinceResponse>;
    attributes(): AttributeResponse | Promise<AttributeResponse>;
    user(): UserResponse | Promise<UserResponse>;
    userId(userId: string): UserIdResponse | Promise<UserIdResponse>;
    post(input: GetPostDto): PostResponse | Promise<PostResponse>;
    postId(postId: string): PostIdResponse | Promise<PostIdResponse>;
}

export interface IMutation {
    updateProfile(userId: string, input: UpdateUserDto): UserUpdateResponse | Promise<UserUpdateResponse>;
    createPost(input: PostCreateDto): PostCreateResponse | Promise<PostCreateResponse>;
    updatePost(postId: string, input: PostUpdateDto): PostUpdateResponse | Promise<PostUpdateResponse>;
    deletePost(postId: string): PostDeleteResponse | Promise<PostDeleteResponse>;
    login(input: LoginDto): LoginSchema | Promise<LoginSchema>;
    register(input: RegisterDto): RegisterSchema | Promise<RegisterSchema>;
}

export type DateTime = any;
type Nullable<T> = T | null;
