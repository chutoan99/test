import { join } from 'path'
import { registerAs } from '@nestjs/config'

export default registerAs('graphql', () => ({
	driver: 'apollo',
	autoSchemaFile: true,
	playground: true,
	definitions: {
		path: join(process.cwd(), 'graphql.ts')
	}
}))
