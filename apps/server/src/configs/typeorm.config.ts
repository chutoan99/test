import { registerAs } from '@nestjs/config'
import { config as dotenvConfig } from 'dotenv'
import { DataSource, DataSourceOptions } from 'typeorm'

dotenvConfig({ path: '.env' })

const dataSourceOptions: DataSourceOptions = {
	type: 'postgres',
	url: process.env.DB_URl,
	entities: ['dist/**/*.entity{.ts,.js}'],
	migrations: ['dist/**/*.migration{.ts,.js}'],
	synchronize: true
}

export default registerAs('typeorm', () => dataSourceOptions)
export const connectionSource = new DataSource(dataSourceOptions as DataSourceOptions)
