import { AreaModule } from '@modules/area/area.module'
import { AreaEntity } from '@modules/area/entitiy'
import { AttributeModule } from '@modules/attribute/attribute.module'
import { AttributeEntity } from '@modules/attribute/entity'
import { AuthModule } from '@modules/auth/auth.module'
import { CategoryModule } from '@modules/category/category.module'
import { CategoryEntity } from '@modules/category/entity'
import { InsertModule } from '@modules/insert/insert.module'
import { LabelEntity } from '@modules/label/entity'
import { LabelModule } from '@modules/label/label.module'
import { OverviewEntity } from '@modules/overview/entity'
import { OverviewModule } from '@modules/overview/overview.module'
import { PostEntity } from '@modules/post/entity'
import { PostModule } from '@modules/post/post.module'
import { PriceEntity } from '@modules/price/entity'
import { PriceModule } from '@modules/price/price.module'
import { ProvinceEntity } from '@modules/province/entity'
import { ProvinceModule } from '@modules/province/province.module'
import { UserEntity } from '@modules/user/entity'
import { UserModule } from '@modules/user/user.module'
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo'
import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { GraphQLModule } from '@nestjs/graphql'
import { TypeOrmModule } from '@nestjs/typeorm'
import graphqlConfig from 'configs/graphql.config'
import typeormConfig from 'configs/typeorm.config'
import { join } from 'path'
@Module({
	imports: [
		ConfigModule.forRoot({ envFilePath: ['.env'] }),

		GraphQLModule.forRoot<ApolloDriverConfig>({
			driver: ApolloDriver,
			autoSchemaFile: true,
			playground: true,
			definitions: {
				path: join(process.cwd(), 'graphql.ts')
			}
		}),

		TypeOrmModule.forRoot({
			type: 'postgres',
			host: process.env.DB_HOST,
			port: +process.env.DB_PORT,
			username: process.env.DB_USERNAME,
			password: process.env.DB_PASSWORD,
			database: process.env.DB_NAME,
			entities: ['dist/**/*.entity{.ts,.js}'],
			migrations: ['dist/**/*.migration{.ts,.js}'],
			synchronize: true,
			ssl: {
				rejectUnauthorized: false, // Set to true if you have a valid SSL certificate
			  },
		}),

		TypeOrmModule.forFeature([
			AreaEntity,
			CategoryEntity,
			PriceEntity,
			ProvinceEntity,
			LabelEntity,
			PostEntity,
			AttributeEntity,
			OverviewEntity,
			UserEntity
		]),

		AreaModule,
		CategoryModule,
		PriceModule,
		ProvinceModule,
		LabelModule,
		AttributeModule,
		OverviewModule,
		UserModule,
		PostModule,
		InsertModule,
		AuthModule
	],
	controllers: [],
	providers: []
})
export class AppModule {}
