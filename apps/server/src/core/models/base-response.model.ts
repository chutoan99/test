import { Field, Int, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class BaseResponse {
	@Field(() => Int)
	err: number

	@Field()
	msg: string
}
