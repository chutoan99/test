import { Column, CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from 'typeorm'

export class BaseEntityModel {
	@Column({ nullable: true })
	isActive: boolean

	@CreateDateColumn()
	createdAt: Date

	@Column({ nullable: true })
	createdBy?: number

	@UpdateDateColumn()
	updatedAt: Date

	@Column({ nullable: true })
	updatedBy?: number

	@DeleteDateColumn()
	deletedAt: Date

	@Column({ nullable: true })
	deletedBy?: number
}
