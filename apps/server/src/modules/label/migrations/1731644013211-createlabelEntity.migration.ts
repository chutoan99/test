import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class CreateEntity1731644013211 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Labels',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						isPrimary: true
					},
					{
						name: 'value',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		// await queryRunner.createForeignKey(
		//   'Labels',
		//   new TableForeignKey({
		//     columnNames: ['id'],
		//     referencedColumnNames: ['label_id'],
		//     referencedTableName: 'Posts',
		//     onDelete: 'CASCADE',
		//   })
		// );
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Labels');
	}
}
