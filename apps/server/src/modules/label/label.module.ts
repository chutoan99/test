import { Module } from '@nestjs/common'
import { LabelService } from './service'

@Module({
	providers: [LabelService]
})
export class LabelModule {}
