import { BaseEntityModel } from '@core/models'
import { PostEntity } from '@modules/post/entity'
import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm'

@Entity('Labels')
export class LabelEntity extends BaseEntityModel {
	@PrimaryColumn()
	id: string

	@Column()
	value: string

	@OneToMany(() => PostEntity, (post: PostEntity) => post.label)
	posts: PostEntity[]
}
