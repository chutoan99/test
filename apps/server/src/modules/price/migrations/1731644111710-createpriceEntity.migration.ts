import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class CreatePriceEntity1731644111710 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Prices',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						isPrimary: true
					},
					{
						name: 'order',
						type: 'int'
					},
					{
						name: 'value',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		// await queryRunner.createForeignKey(
		// 	'Posts',
		// 	new TableForeignKey({
		// 		columnNames: ['priceCode'],
		// 		referencedColumnNames: ['id'],
		// 		referencedTableName: 'Prices',
		// 		onDelete: 'CASCADE'
		// 	})
		// )
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Prices');
	}
}
