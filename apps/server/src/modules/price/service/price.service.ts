import { Inject, Injectable } from '@nestjs/common'
import { catchError, map, Observable, of } from 'rxjs'
import { PriceEntity } from '../entity'
import { PriceResponse } from '../response'
import { IPriceRepository } from '../repository'


export type IPriceService = {
	GetPrices(): Observable<PriceResponse>
}

@Injectable()
export class PriceService implements IPriceService {
	constructor(
		@Inject('IPriceRepository') // Use the injection token
		private readonly _priceRepository: IPriceRepository
	) {}

	public GetPrices(): Observable<PriceResponse> {
		return this._priceRepository.findAll$().pipe(
			map((prices: PriceEntity[]) => {
				return {
					err: 0,
					msg: 'OK',
					response: prices
				}
			}),
			catchError((error) => {
				return of({
					err: 1,
					msg: error.message,
					response: []
				})
			})
		)
	}
}
