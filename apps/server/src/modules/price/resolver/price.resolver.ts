import { Inject } from '@nestjs/common'
import { Query, Resolver } from '@nestjs/graphql'
import { Observable } from 'rxjs'
import { PriceResponse } from '../response'
import { IPriceService } from '../service'

@Resolver()
export class PriceResolver {
	constructor(
		@Inject('IPriceService') // Use the injection token
		private readonly _priceService: IPriceService
	) {}

	@Query(() => PriceResponse)
	public price(): Observable<PriceResponse> {
		return this._priceService.GetPrices()
	}
}
