import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PriceEntity } from './entity'
import { PriceRepository } from './repository'
import { PriceResolver } from './resolver'
import { PriceService } from './service'

@Module({
	imports: [TypeOrmModule.forFeature([PriceEntity])],
	providers: [
		PriceResolver,
		{
			provide: 'IPriceRepository',
			useClass: PriceRepository
		},
		{
			provide: 'IPriceService',
			useClass: PriceService
		}
	]
})
export class PriceModule {}
