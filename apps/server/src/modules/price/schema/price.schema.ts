import { Field, Int, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class PriceSchema {
	@Field()
	id: string

	@Field(() => Int)
	order: number

	@Field()
	value: string

	@Field(() => Date)
	createdAt: Date

	@Field(() => Date)
	updatedAt: Date
}
