import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { PriceSchema } from '../schema'

@ObjectType()
export class PriceResponse extends BaseResponse {
	@Field(() => [PriceSchema])
	response: PriceSchema[] | []
}
