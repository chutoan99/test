import { BaseEntityModel } from '@core/models'
import { PostEntity } from '@modules/post/entity'
import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm'

@Entity('Prices')
export class PriceEntity extends BaseEntityModel {
	@PrimaryColumn()
	id: string

	@Column()
	order: number

	@Column()
	value: string

	@OneToMany(() => PostEntity, (post: PostEntity) => post.price)
	posts: PostEntity[]
}
