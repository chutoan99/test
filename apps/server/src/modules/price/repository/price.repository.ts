import { InjectRepository } from '@nestjs/typeorm'
import { from, Observable } from 'rxjs'
import { Repository } from 'typeorm'
import { PriceEntity } from '../entity'

export type IPriceRepository = {
	findAll$(): Observable<PriceEntity[]>
	bulkCreate(prices: any[])
}

export class PriceRepository implements IPriceRepository {
	constructor(
		@InjectRepository(PriceEntity)
		private readonly _priceRepository: Repository<PriceEntity>
	) {}

	public findAll$(): Observable<PriceEntity[]> {
		return from(this._priceRepository.find({ where: { isActive: true } }))
	}

	public async bulkCreate(prices: any[]) {
		try {
			return await this._priceRepository.save(
				prices.map((item, index) => ({
					id: item?.code,
					order: index + 1,
					value: item?.value,
					isActive: true
				}))
			)
		} catch (err) {
			console.log(err)
		}
	}
}
