import { PostSchema } from '@modules/post/schema'
import { Field, ID, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class UserSchema {
	@Field(() => ID)
	id: string

	@Field()
	name: string

	@Field()
	password: string

	@Field({ nullable: true })
	avatar: string

	@Field({ nullable: true })
	phone: string

	@Field({ nullable: true })
	zalo: string | null

	@Field({ nullable: true })
	file: string | null

	@Field(() => [PostSchema], { nullable: true })
	post: PostSchema[] | null // Allowing null here

	@Field(() => Date)
	createdAt: Date

	@Field(() => Date)
	updatedAt: Date
}
