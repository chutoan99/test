import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class CreateUserEntity1731644217369 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Users',
				columns: [
					{
						name: 'id',
						type: 'uuid',
						isPrimary: true,
						default: 'gen_random_uuid()',
					},
					{
						name: 'name',
						type: 'varchar'
					},
					{
						name: 'password',
						type: 'varchar'
					},
					{
						name: 'phone',
						type: 'varchar'
					},
					{
						name: 'zalo',
						type: 'varchar',
						isNullable: true
					},
					{
						name: 'file',
						type: 'varchar',
						isNullable: true
					},
					{
						name: 'avatar',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Users')
	}
}
