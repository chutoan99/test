import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { UserSchema } from '../schema'

@ObjectType()
export class UserResponse extends BaseResponse {
	@Field(() => [UserSchema])
	response: UserSchema[]
}
