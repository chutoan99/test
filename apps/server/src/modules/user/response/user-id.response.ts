import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { UserSchema } from '../schema'

@ObjectType()
export class UserIdResponse extends BaseResponse {
	@Field(() => UserSchema)
	response: UserSchema
}
