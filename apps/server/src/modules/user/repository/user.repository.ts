import { InjectRepository } from '@nestjs/typeorm'
import { Repository, UpdateResult } from 'typeorm'
import { UpdateUserDto } from '../dto'
import { UserEntity } from '../entity'

export type IUserRepository = {
	findAll(): Promise<UserEntity[]>
	findById(id: string): Promise<UserEntity>
	updateUser(id: string, payload: UpdateUserDto): Promise<boolean>
}


export class UserRepository implements IUserRepository {
	constructor(
		@InjectRepository(UserEntity)
		private readonly _userRepository: Repository<UserEntity>
	) {}

	public async findAll(): Promise<UserEntity[]> {
		return await this._userRepository.find({
			where: { isActive: true }
		})
	}

	public async findById(id: string): Promise<UserEntity> {
		return await this._userRepository.findOne({
			where: { id, isActive: true }
		})
	}

	public async updateUser(id: string, payload: UpdateUserDto): Promise<boolean> {
		const response: UpdateResult = await this._userRepository.update(id, {
			name: payload.name,
			avatar: payload.avatar,
			phone: payload.phone,
			zalo: payload.zalo,
			file: payload.file
		})

		return response.affected > 0
	}
}
