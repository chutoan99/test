import { PostEntity } from '@modules/post/entity'
import { PostSchema } from '@modules/post/schema'
import { Inject } from '@nestjs/common'
import { Args, ID, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { UpdateUserDto } from '../dto'
import { UserIdResponse, UserResponse, UserUpdateResponse } from '../response'
import { UserSchema } from '../schema'
import { IUserService } from '../service'

@Resolver(() => UserSchema)
export class UserResolver {
	constructor(
		@Inject('IUserService') // Use the injection token
		private readonly _userService: IUserService,

		@InjectRepository(PostEntity)
		private readonly _postRepository: Repository<PostEntity>
	) {}

	@Query(() => UserResponse)
	public user() {
		return this._userService.getAllUser()
	}

	@Query(() => UserIdResponse)
	public userId(
		@Args('userId', {
			type: () => ID
		})
		id: string
	) {
		return this._userService.getCurrentUser(id)
	}

	@Mutation(() => UserUpdateResponse)
	public updateProfile(
		@Args('userId', {
			type: () => ID
		})
		id: string,
		@Args('input', {
			type: () => UpdateUserDto
		})
		input: UpdateUserDto
	): Promise<UserUpdateResponse> {
		return this._userService.updateUser(id, input)
	}

	@ResolveField(() => PostSchema)
	public post(@Parent() user: UserSchema): Promise<PostEntity[]> {
		return this._postRepository.find({
			where: { userId: user.id, isActive: true }
		})
	}
}
