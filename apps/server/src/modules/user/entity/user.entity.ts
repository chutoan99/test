import { BaseEntityModel } from '@core/models'
import { PostEntity } from '@modules/post/entity'
import { Column, Entity, OneToOne, PrimaryColumn } from 'typeorm'
import { v4 as uuidv4 } from 'uuid'

@Entity('Users')
export class UserEntity extends BaseEntityModel {
	@PrimaryColumn('uuid', { default: uuidv4() })
	id: string

	@Column()
	name: string

	@Column()
	password: string

	@Column()
	phone: string

	@Column({ nullable: true })
	zalo: string

	@Column({ nullable: true })
	file: string

	@Column()
	avatar: string

	@OneToOne(() => PostEntity, (post: PostEntity) => post.user)
	posts: PostEntity[]
}
