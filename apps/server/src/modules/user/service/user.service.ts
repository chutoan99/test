import { Inject, Injectable } from '@nestjs/common'
import { UpdateUserDto } from '../dto'
import { UserEntity } from '../entity'
import { UserUpdateResponse } from '../response'
import { IUserRepository } from '../repository'

export type IUserService = {
	getAllUser()
	getCurrentUser(id: string)
	updateUser(id: string, input: UpdateUserDto): Promise<UserUpdateResponse>
}


@Injectable()
export class UserService implements IUserService {
	constructor(
		@Inject('IUserRepository') // Use the injection token
		private readonly _userRepository: IUserRepository
	) {}

	async getAllUser() {
		const response: UserEntity[] = await this._userRepository.findAll()
		return {
			err: response ? 0 : 1,
			msg: response ? 'OK' : 'Failed to get user.',
			response
		}
	}

	async getCurrentUser(id: string) {
		const response: UserEntity = await this._userRepository.findById(id)
		return {
			err: response ? 0 : 1,
			msg: response ? 'OK' : 'Failed to get userid',
			response
		}
	}

	async updateUser(id: string, input: UpdateUserDto): Promise<UserUpdateResponse> {
		try {
			const isUpdated: boolean = await this._userRepository.updateUser(id, input)
			if (isUpdated) {
				return {
					err: 0,
					msg: 'updated Profile successfully'
				}
			} else {
				return {
					err: 0,
					msg: 'updated Profile fail'
				}
			}
		} catch (error) {
			return {
				err: -1,
				msg: error.toString()
			}
		}
	}
}
