import { AttributeEntity } from '@modules/attribute/entity'
import { LabelEntity } from '@modules/label/entity'
import { OverviewEntity } from '@modules/overview/entity'
import { PostEntity } from '@modules/post/entity'
import { ProvinceEntity } from '@modules/province/entity'
import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from './entity'
import { UserRepository } from './repository'
import { UserResolver } from './resolver/user.resolver'
import { UserService } from './service'

@Module({
	imports: [
		TypeOrmModule.forFeature([UserEntity, ProvinceEntity, AttributeEntity, LabelEntity, PostEntity, OverviewEntity])
	],
	providers: [
		UserResolver,
		{
			provide: 'IUserRepository',
			useClass: UserRepository
		},
		{
			provide: 'IUserService',
			useClass: UserService
		}
	]
})
export class UserModule {}
