import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { ProvinceSchema } from '../schema'

@ObjectType()
export class ProvinceResponse extends BaseResponse {
	@Field(() => [ProvinceSchema])
	response: ProvinceSchema[]
}
