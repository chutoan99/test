import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ProvinceEntity } from './entity'
import { ProvinceRepository } from './repository'
import { ProvinceResolver } from './resolver'
import { ProvinceService } from './service'

@Module({
	imports: [TypeOrmModule.forFeature([ProvinceEntity])],
	providers: [
		ProvinceResolver,
		{
			provide: 'IProvinceRepository',
			useClass: ProvinceRepository
		},
		{
			provide: 'IProvinceService',
			useClass: ProvinceService
		}
	]
})
export class ProvinceModule {}
