import { InjectRepository } from '@nestjs/typeorm'
import { from, Observable } from 'rxjs'
import { Repository } from 'typeorm'
import { ProvinceEntity } from '../entity'

export type IProvinceRepository = {
	findAll$(): Observable<ProvinceEntity[]>
}


export class ProvinceRepository implements IProvinceRepository {
	constructor(
		@InjectRepository(ProvinceEntity)
		private readonly _provinceRepository: Repository<ProvinceEntity>
	) {}

	public findAll$(): Observable<ProvinceEntity[]> {
		return from(this._provinceRepository.find({ where: { isActive: true } }))
	}

	public async findById(id: string): Promise<ProvinceEntity> {
		return await this._provinceRepository.findOneBy({ id })
	}
}
