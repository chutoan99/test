import { Inject } from '@nestjs/common'
import { Query, Resolver } from '@nestjs/graphql'
import { Observable } from 'rxjs'
import { ProvinceResponse } from '../response'
import { IProvinceService } from '../service'

@Resolver()
export class ProvinceResolver {
	constructor(
		@Inject('IProvinceService') // Use the injection token
		private readonly _provinceService: IProvinceService
	) {}

	@Query(() => ProvinceResponse)
	public province(): Observable<ProvinceResponse> {
		return this._provinceService.GetProvinces()
	}
}
