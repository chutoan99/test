import { ObjectType, Field, ID } from '@nestjs/graphql'

@ObjectType()
export class ProvinceSchema {
	@Field(() => ID)
	id: string

	@Field()
	value: string

	@Field(() => Date)
	createdAt: Date

	@Field(() => Date)
	updatedAt: Date
}
