import { Inject, Injectable } from '@nestjs/common'
import { catchError, map, Observable, of } from 'rxjs'
import { ProvinceEntity } from '../entity'
import { ProvinceResponse } from '../response'
import { IProvinceRepository } from '../repository'

export type IProvinceService = {
	GetProvinces(): Observable<ProvinceResponse>
}
@Injectable()
export class ProvinceService implements IProvinceService {
	constructor(
		@Inject('IProvinceRepository') // Use the injection token
		private readonly _provinceRepository: IProvinceRepository
	) {}

	public GetProvinces(): Observable<ProvinceResponse> {
		return this._provinceRepository.findAll$().pipe(
			map((prices: ProvinceEntity[]) => {
				return {
					err: 0,
					msg: 'OK',
					response: prices
				}
			}),
			catchError((error) => {
				return of({
					err: 1,
					msg: error.message,
					response: []
				})
			})
		)
	}
}
