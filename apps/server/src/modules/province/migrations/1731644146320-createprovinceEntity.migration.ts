import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class CreateProvinceEntity1731644146320 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Provinces',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						isPrimary: true
					},
					{
						name: 'value',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		// Tạo khóa ngoại cho `Posts` trỏ đến `Provinces`
		// await queryRunner.createForeignKey(
		// 	'Posts',
		// 	new TableForeignKey({
		// 		columnNames: ['provinceCode'],
		// 		referencedColumnNames: ['id'],
		// 		referencedTableName: 'Provinces',
		// 		onDelete: 'CASCADE'
		// 	})
		// )
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Provinces');
	}
}
