import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { OverviewEntity } from './entity'
import { OverviewRepository } from './repository'

@Module({
	imports: [TypeOrmModule.forFeature([OverviewEntity])],
	providers: [
		{
			provide: 'IOverviewRepository',
			useClass: OverviewRepository
		}
	]
})
export class OverviewModule {}
