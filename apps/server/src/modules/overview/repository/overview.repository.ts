import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { OverviewEntity } from '../entity'

export type IOverviewRepository = {
	findOne(overviewId: string): Promise<OverviewEntity>
}


export class OverviewRepository implements IOverviewRepository {
	constructor(
		@InjectRepository(OverviewEntity)
		private readonly _overviewRepository: Repository<OverviewEntity>
	) {}

	public async findOne(overviewId: string): Promise<OverviewEntity> {
		return await this._overviewRepository.findOne({
			where: { id: overviewId, isActive: true }
		})
	}
}
