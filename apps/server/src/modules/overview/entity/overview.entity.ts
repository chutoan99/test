import { BaseEntityModel } from '@core/models'
import { PostEntity } from '@modules/post/entity'
import { Column, Entity, OneToOne, PrimaryColumn } from 'typeorm'
import { v4 as uuidv4 } from 'uuid'

@Entity('Overviews')
export class OverviewEntity extends BaseEntityModel {
	@PrimaryColumn('uuid', { default: uuidv4() })
	id: string

	@Column()
	code: string

	@Column()
	area: string

	@Column()
	type: string

	@Column()
	target: string

	@Column()
	created: string

	@Column()
	expired: string

	@Column()
	bonus: string

	@OneToOne(() => PostEntity, (post: PostEntity) => post.overview)
	post: PostEntity
}
