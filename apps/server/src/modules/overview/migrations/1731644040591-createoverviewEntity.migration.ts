import { Table } from 'typeorm'
import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm'

export class CreateOverviewEntity1731644040591 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Overviews',
				columns: [
					{
						name: 'id',
						type: 'uuid',
						isPrimary: true,
						default: 'gen_random_uuid()',
					},
					{
						name: 'code',
						type: 'varchar'
					},
					{
						name: 'area',
						type: 'varchar'
					},
					{
						name: 'type',
						type: 'varchar'
					},
					{
						name: 'target',
						type: 'varchar'
					},
					{
						name: 'created',
						type: 'varchar'
					},
					{
						name: 'expired',
						type: 'varchar'
					},
					{
						name: 'bonus',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		// await queryRunner.createForeignKey(
		// 	'Overviews',
		// 	new TableForeignKey({
		// 		columnNames: ['id'],
		// 		referencedColumnNames: ['overview_id'],
		// 		referencedTableName: 'Posts',
		// 		onDelete: 'CASCADE'
		// 	})
		// )
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Overviews');
	}
}
