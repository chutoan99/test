import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class CreateAreaEntity1731642728272 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Areas',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						isPrimary: true
					},
					{
						name: 'order',
						type: 'int'
					},
					{
						name: 'value',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		// Create foreign key for Posts table
		// await queryRunner.createForeignKey(
		// 	'Posts',
		// 	new TableForeignKey({
		// 		columnNames: ['areaCode'],
		// 		referencedTableName: 'Areas',
		// 		referencedColumnNames: ['id'],
		// 		onDelete: 'CASCADE'
		// 	})
		// )
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Areas')
	}
}
