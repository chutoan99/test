import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AreaEntity } from './entitiy'
import { AreaResolver } from './resolver'
import { AreaRepository } from './repository'
import { AreaService } from './service'

@Module({
	imports: [TypeOrmModule.forFeature([AreaEntity])],
	providers: [
		AreaResolver,
		{
			provide: 'IAreaRepository',
			useClass: AreaRepository
		},
		{
			provide: 'IAreaService',
			useClass: AreaService
		}
	]
})
export class AreaModule {}
