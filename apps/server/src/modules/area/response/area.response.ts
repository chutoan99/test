import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { AreaSchema } from '../schema'

@ObjectType()
export class AreaResponse extends BaseResponse {
	@Field(() => [AreaSchema])
	response: AreaSchema[]
}
