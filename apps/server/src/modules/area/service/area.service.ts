import { Inject, Injectable } from '@nestjs/common'
import { catchError, map, Observable, of } from 'rxjs'
import { AreaEntity } from '../entitiy'
import { AreaResponse } from '../response'
import { IAreaRepository } from '../repository'

export type IAreaService = {
	getAreas(): Observable<AreaResponse>
}

@Injectable()
export class AreaService implements IAreaService {
	constructor(
		@Inject('IAreaRepository') // Use the injection token
		private readonly _areaRepository: IAreaRepository
	) {}

	public getAreas() {
		return this._areaRepository.findAll$().pipe(
			map((areas: AreaEntity[]) => {
				return {
					err: 0,
					msg: 'OK',
					response: areas
				}
			}),
			catchError((error) => {
				return of({
					err: 1,
					msg: error.message,
					response: []
				})
			})
		)
	}
}
