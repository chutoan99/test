import { Inject } from '@nestjs/common'
import { Query, Resolver } from '@nestjs/graphql'
import { Observable } from 'rxjs'
import { AreaResponse } from '../response'
import { IAreaService } from '../service'

@Resolver()
export class AreaResolver {
	constructor(
		@Inject('IAreaService') // Use the injection token
		private readonly _areaService: IAreaService
	) {}

	@Query(() => AreaResponse)
	public area(): Observable<AreaResponse> {
		return this._areaService.getAreas()
	}
}
