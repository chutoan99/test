import { InjectRepository } from '@nestjs/typeorm'
import { from, Observable } from 'rxjs'
import { Repository } from 'typeorm'
import { AreaEntity } from '../entitiy'

export type IAreaRepository = {
	findAll$(): Observable<AreaEntity[]>
	bulkCreate(areas: any[])
}


export class AreaRepository implements IAreaRepository {
	constructor(
		@InjectRepository(AreaEntity)
		private readonly _areaRepository: Repository<AreaEntity>
	) {}

	public findAll$(): Observable<AreaEntity[]> {
		return from(this._areaRepository.find({ where: { isActive: true } }))
	}

	public async bulkCreate(areas: any[]) {
		try {
			return await this._areaRepository.save(
				areas.map((area, index) => ({
					id: area.code,
					order: index + 1,
					value: area.value,
					isActive: true
				}))
			)
		} catch (err) {
			console.log(err)
		}
	}
}
