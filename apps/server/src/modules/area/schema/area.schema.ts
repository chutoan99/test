import { ObjectType, Field, Int } from '@nestjs/graphql'
@ObjectType()
export class AreaSchema {
	@Field()
	id: string

	@Field(() => Int)
	order: number

	@Field()
	value: string

	@Field(() => Date)
	createdAt: Date

	@Field(() => Date)
	updatedAt: Date
}
