import { Inject } from '@nestjs/common'
import { Query, Resolver } from '@nestjs/graphql'
import { Observable } from 'rxjs'
import { AttributeResponse } from '../response'
import { IAttributeService } from '../service'

@Resolver()
export class AttributeResolver {
	constructor(
		@Inject('IAttributeService') // Use the injection token
		private readonly _attributeService: IAttributeService
	) {}

	@Query(() => AttributeResponse)
	public attributes(): Observable<AttributeResponse> {
		return this._attributeService.getAttributes()
	}
}
