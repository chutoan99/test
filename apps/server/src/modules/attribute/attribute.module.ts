import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AttributeEntity } from './entity'
import { AttributeRepository } from './repository'
import { AttributeResolver } from './resolver'
import { AttributeService } from './service'

@Module({
	imports: [TypeOrmModule.forFeature([AttributeEntity])],
	controllers: [],
	providers: [
		AttributeResolver,
		{
			provide: 'IAttributeRepository',
			useClass: AttributeRepository
		},
		{
			provide: 'IAttributeService',
			useClass: AttributeService
		}
	]
})
export class AttributeModule {}
