import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { AttributeSchema } from '../schema'

@ObjectType()
export class AttributeResponse extends BaseResponse {
	@Field(() => [AttributeSchema])
	response: AttributeSchema[]
}
