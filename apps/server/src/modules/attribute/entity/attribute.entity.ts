import { BaseEntityModel } from '@core/models'
import { PostEntity } from '@modules/post/entity'
import { Column, Entity, OneToOne, PrimaryColumn } from 'typeorm'
import { v4 as uuidv4 } from 'uuid'

@Entity('Attributes')
export class AttributeEntity extends BaseEntityModel {
	@PrimaryColumn('uuid', { default: uuidv4() })
	id: string

	@Column()
	price: string

	@Column()
	acreage: string

	@Column()
	published: string

	@Column()
	hashtag: string

	@OneToOne(() => PostEntity, (post: PostEntity) => post.attribute)
	post: PostEntity
}
