import { Inject, Injectable } from '@nestjs/common'
import { catchError, map, of } from 'rxjs'
import { AttributeEntity } from '../entity'
import { IAttributeRepository } from '../repository'


export type IAttributeService = {
	getAttributes()
}


@Injectable()
export class AttributeService implements IAttributeService {
	constructor(
		@Inject('IAttributeRepository') // Use the injection token
		private readonly _attributeRepository: IAttributeRepository
	) {}

	public getAttributes() {
		return this._attributeRepository.findAll$().pipe(
			map((attributes: AttributeEntity[]) => {
				return {
					err: 0,
					msg: 'OK',
					response: attributes
				}
			}),
			catchError((error) => {
				return of({
					err: 1,
					msg: error.message,
					response: []
				})
			})
		)
	}
}
