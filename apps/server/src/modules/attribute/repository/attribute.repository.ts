import { InjectRepository } from '@nestjs/typeorm'
import { from, Observable } from 'rxjs'
import { Repository } from 'typeorm'
import { AttributeEntity } from '../entity'

export type IAttributeRepository = {
	findAll$(): Observable<AttributeEntity[]>
}
export class AttributeRepository implements IAttributeRepository {
	constructor(
		@InjectRepository(AttributeEntity)
		private readonly _attributeRepository: Repository<AttributeEntity>
	) {}

	public findAll$(): Observable<AttributeEntity[]> {
		return from(this._attributeRepository.find({ where: { isActive: true } }))
	}
}
