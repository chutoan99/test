import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class CreateAttributeEntity1731643972650 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Attributes',
				columns: [
					{
						name: 'id',
						type: 'uuid',
						isPrimary: true,
						default: 'gen_random_uuid()',
					},
					{
						name: 'price',
						type: 'varchar'
					},
					{
						name: 'acreage',
						type: 'varchar'
					},
					{
						name: 'published',
						type: 'varchar'
					},
					{
						name: 'hashtag',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		// await queryRunner.createForeignKey(
		//   'Attributes',
		//   new TableForeignKey({
		//     columnNames: ['id'],
		//     referencedColumnNames: ['attribute_id'],
		//     referencedTableName: 'Posts',
		//     onDelete: 'CASCADE',
		//   })
		// );
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Attributes');
	}
}
