import { UserEntity } from '@modules/user/entity'
import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { LoginDto, RegisterDto } from './dto'
import { AuthResolver } from './resolver'
import { AuthService } from './service'

@Module({
	imports: [TypeOrmModule.forFeature([UserEntity])],
	controllers: [],
	providers: [
		AuthResolver,
		RegisterDto,
		LoginDto,
		{
			provide: 'IAuthService',
			useClass: AuthService
		}
	]
})
export class AuthModule {}
