import { Field, Int, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class RegisterSchema {
	@Field(() => Int)
	err: number

	@Field()
	msg: string

	@Field({ nullable: true })
	token: string
}
