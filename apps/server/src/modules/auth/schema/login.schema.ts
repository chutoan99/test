import { UserSchema } from '@modules/user/schema'
import { Field, Int, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class LoginSchema {
	@Field(() => Int)
	err: number

	@Field()
	msg: string

	@Field({ nullable: true })
	token: string

	@Field(() => UserSchema)
	response: UserSchema
}
