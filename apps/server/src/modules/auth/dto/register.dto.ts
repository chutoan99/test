import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class RegisterDto {
	@Field(() => String)
	name: string

	@Field(() => String)
	phone: string

	@Field(() => String)
	password: string
}
