import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class LoginDto {
	@Field(() => String)
	phone: string

	@Field(() => String)
	password: string
}
