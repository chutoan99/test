import { Inject } from '@nestjs/common'
import { Args, Mutation, Resolver } from '@nestjs/graphql'
import { LoginDto, RegisterDto } from '../dto'
import { LoginSchema, RegisterSchema } from '../schema'
import { UserSchema } from '@modules/user/schema'
import { IAuthService } from '../service'

@Resolver(() => UserSchema)
export class AuthResolver {
	constructor(
		@Inject('IAuthService')
		private readonly _authService: IAuthService
	) {}

	@Mutation(() => LoginSchema)
	public login(
		@Args('input', {
			type: () => LoginDto
		})
		input: LoginDto
	) {
		return this._authService.login(input)
	}

	@Mutation(() => RegisterSchema)
	public register(
		@Args('input', {
			type: () => RegisterDto
		})
		input: RegisterDto
	) {
		return this._authService.register(input)
	}
}
