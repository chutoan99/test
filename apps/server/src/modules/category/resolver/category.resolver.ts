import { Inject } from '@nestjs/common'
import { Query, Resolver } from '@nestjs/graphql'
import { CategoryResponse } from '../response'
import { ICategoryService } from '../service'

@Resolver()
export class CategoryResolver {
	constructor(
		@Inject('ICategoryService')
		private readonly _categoryService: ICategoryService
	) {}

	@Query(() => CategoryResponse)
	public category(): Promise<CategoryResponse> {
		return this._categoryService.GetAllCategory()
	}
}
