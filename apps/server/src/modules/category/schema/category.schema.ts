import { ObjectType, Field, ID, Int } from '@nestjs/graphql'

@ObjectType()
export class CategorySchema {
	@Field(() => ID)
	id: string

	@Field()
	value: string

	@Field()
	header: string

	@Field()
	subHeader: string

	@Field()
	path: string

	@Field(() => Date)
	createdAt: Date

	@Field(() => Date)
	updatedAt: Date
}
