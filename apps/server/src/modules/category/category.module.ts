import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CategoryEntity } from './entity'
import { CategoryRepository } from './repository'
import { CategoryResolver } from './resolver'
import { CategoryService } from './service'

@Module({
	imports: [TypeOrmModule.forFeature([CategoryEntity])],
	controllers: [],
	providers: [
		CategoryResolver,
		{
			provide: 'ICategoryRepository',
			useClass: CategoryRepository
		},
		{
			provide: 'ICategoryService',
			useClass: CategoryService
		}
	]
})
export class CategoryModule {}
