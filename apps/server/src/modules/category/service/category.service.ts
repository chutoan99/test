import { Inject, Injectable } from '@nestjs/common'
import { CategoryEntity } from '../entity'
import { CategoryResponse } from '../response'
import { ICategoryRepository } from '../repository'


export type ICategoryService = {
	GetAllCategory(): Promise<CategoryResponse>
}


@Injectable()
export class CategoryService implements ICategoryService {
	constructor(
		@Inject('ICategoryRepository')
		private readonly _categoryRepository: ICategoryRepository
	) {}

	async GetAllCategory(): Promise<CategoryResponse> {
		const response: CategoryEntity[] = await this._categoryRepository.findAll()
		return {
			err: response ? 0 : 1,
			msg: response ? 'OK' : 'Failed to get category.',
			response
		}
	}
}
