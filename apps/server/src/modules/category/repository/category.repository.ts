import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CategoryEntity } from '../entity'

export type ICategoryRepository = {
	findAll(): Promise<CategoryEntity[]>
	bulkCreate(categories: any[])
}

export class CategoryRepository implements ICategoryRepository {
	constructor(
		@InjectRepository(CategoryEntity)
		private readonly _categoryRepository: Repository<CategoryEntity>
	) {}

	async findAll(): Promise<CategoryEntity[]> {
		return await this._categoryRepository.find({
			where: { isActive: true }
		})
	}

	public async bulkCreate(categories: any[]) {
		try {
			return await this._categoryRepository.save(
				categories.map((item) => ({
					id: item.code,
					value: item.value,
					header: item.content.title,
					subHeader: item.content.description,
					path: item.path,
					isActive: true
				}))
			)
		} catch (err) {
			console.log(err)
		}
	}
}
