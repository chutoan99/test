import { BaseEntityModel } from '@core/models'
import { PostEntity } from '@modules/post/entity'
import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm'

@Entity('Categories')
export class CategoryEntity extends BaseEntityModel {
	@PrimaryColumn()
	id: string

	@Column()
	value: string

	@Column()
	header: string

	@Column()
	subHeader: string

	@Column()
	path: string

	@OneToMany(() => PostEntity, (post: PostEntity) => post.category)
	posts: PostEntity[]
}
