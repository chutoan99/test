import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class CreateCategoryEntity1731643993152 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Categories',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						isPrimary: true
					},
					{
						name: 'value',
						type: 'varchar'
					},
					{
						name: 'header',
						type: 'varchar'
					},
					{
						name: 'subHeader',
						type: 'varchar'
					},
					{
						name: 'path',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		// await queryRunner.createForeignKey(
		//   'Categories',
		//   new TableForeignKey({
		//     columnNames: ['id'],
		//     referencedColumnNames: ['category_id'],
		//     referencedTableName: 'Posts',
		//     onDelete: 'CASCADE',
		//   })
		// );
	}
	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Categories');
	}
}
