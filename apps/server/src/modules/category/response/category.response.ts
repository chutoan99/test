import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { CategorySchema } from '../schema'

@ObjectType()
export class CategoryResponse extends BaseResponse {
	@Field(() => [CategorySchema])
	response: CategorySchema[]
}
