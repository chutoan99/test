import { Controller, Inject, Post } from '@nestjs/common'
import { IInsertService } from '../service'

@Controller('insert')
export class InsertController {
	constructor(
		@Inject('IInsertService')
		private readonly _insertService: IInsertService
	) {}

	@Post()
	async insertData(): Promise<string> {
		try {
			const result = await this._insertService.insertData()
			return result
		} catch (error) {
			console.error('Error inserting data:', error)
			throw new Error('Data insertion failed')
		}
	}
}
