import { AreaEntity } from '@modules/area/entitiy'
import { AreaRepository } from '@modules/area/repository'
import { AttributeEntity } from '@modules/attribute/entity'
import { CategoryEntity } from '@modules/category/entity'
import { CategoryRepository } from '@modules/category/repository'
import { LabelEntity } from '@modules/label/entity'
import { OverviewEntity } from '@modules/overview/entity'
import { PostEntity } from '@modules/post/entity'
import { PriceEntity } from '@modules/price/entity'
import { PriceRepository } from '@modules/price/repository'
import { ProvinceEntity } from '@modules/province/entity'
import { UserEntity } from '@modules/user/entity'
import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { InsertController } from './controller'
import { InsertService } from './service'

@Module({
	imports: [
		TypeOrmModule.forFeature([
			AreaEntity,
			CategoryEntity,
			PriceEntity,
			ProvinceEntity,
			LabelEntity,
			PostEntity,
			AttributeEntity,
			OverviewEntity,
			UserEntity
		])
	],
	controllers: [InsertController],
	providers: [
		{
			provide: 'IInsertService',
			useClass: InsertService
		},
		{
			provide: 'IPriceRepository',
			useClass: PriceRepository
		},
		{
			provide: 'ICategoryRepository',
			useClass: CategoryRepository
		},
		{
			provide: 'IAreaRepository',
			useClass: AreaRepository
		}
	]
})
export class InsertModule {}
