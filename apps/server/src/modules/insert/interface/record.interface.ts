export interface IResources {
	header: {
		title: string
		description: string
	}
	body: {
		images: (null | string)[]
		header: {
			title: string
			star: string
			class: {
				content: string
				classType: string
			}
			address: string
			attributes: {
				price: string
				acreage: string
				published: string
				hashtag: string
			}
		}
		mainContent: {
			header: string
			content: string[]
		}
		overview: {
			header: string
			content: {
				name: string
				content: string
			}[]
		}

		contact: {
			header: string
			content: {
				name: string
				content: string
			}[]
		}
	}[]
}
