import * as RENTAL_APARTMENT from '../data/chothuecanho.json'
import * as RENTAL_GROUND from '../data/chothuematbang.json'
import * as RENTAL_MOTEL from '../data/chothuephongtro.json'
import * as RENTAL_HOUSE from '../data/nhachothue.json'
import * as ROOM_MATE from '../data/timnguoioghep.json'

import { AttributeEntity } from '@modules/attribute/entity'
import { LabelEntity } from '@modules/label/entity'
import { OverviewEntity } from '@modules/overview/entity'
import { PostEntity } from '@modules/post/entity'
import { ProvinceEntity } from '@modules/province/entity'
import { Inject, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { getNumberFromString, getNumberFromStringV2 } from '@utils/comom'
import generateCode from '@utils/generateCode'
import * as bcrypt from 'bcryptjs'
import { Repository } from 'typeorm'
import { v4 as uuidv4 } from 'uuid'
import { IResources } from '../interface'
import { DATA_AREA, DATA_PRICE } from '../resource'
import { UserEntity } from '@modules/user/entity'
import { IAreaRepository } from '@modules/area/repository'
import { ICategoryRepository } from '@modules/category/repository'
import { IPriceRepository } from '@modules/price/repository'

export type IInsertService = {
	insertData()
}

@Injectable()
export class InsertService implements IInsertService {
	public _data: Record<string, any> = {}
	private _rentalApartment: any = RENTAL_APARTMENT
	private _rentalGround: any = RENTAL_GROUND
	private _rentalMotel: any = RENTAL_MOTEL
	private _rentalHouse: any = RENTAL_HOUSE
	private _roommate: any = ROOM_MATE

	private _dataPrice: any = DATA_PRICE

	private _dataArea: any = DATA_AREA

	constructor(
		@Inject('IAreaRepository')
		private readonly _areaRepository: IAreaRepository,

		@Inject('ICategoryRepository')
		private readonly _categoryRepository: ICategoryRepository,

		@Inject('IPriceRepository')
		private readonly _priceRepository: IPriceRepository,

		@InjectRepository(ProvinceEntity)
		private readonly _provinceRepository: Repository<ProvinceEntity>,

		@InjectRepository(AttributeEntity)
		private readonly _attributeRepository: Repository<AttributeEntity>,

		@InjectRepository(LabelEntity)
		private readonly _labelRepository: Repository<LabelEntity>,

		@InjectRepository(PostEntity)
		private readonly _postRepository: Repository<PostEntity>,

		@InjectRepository(OverviewEntity)
		private readonly _overviewRepository: Repository<OverviewEntity>,

		@InjectRepository(UserEntity)
		private readonly _userRepository: Repository<UserEntity>
	) {}

	async insertData() {
		console.log(this._rentalApartment, '_rentalApartment')
		try {
			const hashPassWord = (password) => bcrypt.hashSync(password, bcrypt.genSaltSync(12)) // HASH PASSWORD

			this._dataArea = this._dataArea.map((item) => ({
				...item,
				code: generateCode(item.value)
			}))

			this._dataPrice = this._dataPrice.map((item) => ({
				...item,
				code: generateCode(item.value)
			}))

			await this._areaRepository.bulkCreate(this._dataArea)

			await this._categoryRepository.bulkCreate([
				{
					code: 'CTCH',
					value: 'Cho Thuê Căn Hộ',
					content: this._rentalApartment?.header,
					path: 'rental-apartment'
				},
				{
					code: 'CTMB',
					value: 'Cho Thuê Mặt Bằng',
					content: this._rentalGround?.header,
					path: 'rental-ground'
				},
				{
					code: 'CTPT',
					value: 'Cho Thuê Phòng Trọ',
					content: this._rentalMotel?.header,
					path: 'rental-motel'
				},
				{
					code: 'NCT',
					value: 'Nhà cho thuê',
					content: this._rentalHouse?.header,
					path: 'rental-house'
				},
				{
					code: 'TNOG',
					value: 'Tìm Người Ở Ghép',
					content: this._roommate?.header,
					path: 'roommate'
				},
				{
					code: 'BLOG',
					value: 'Blog',
					content: { title: '', description: '' },
					path: 'blog'
				},
				{
					code: 'HUDA',
					value: 'Hướng Dẫn',
					content: { title: '', description: '' },
					path: 'user-manal'
				},
				{
					code: 'NATI',
					value: 'Nạp Tiền',
					content: { title: '', description: '' },
					path: 'money'
				},
				{
					code: 'BAGI',
					value: 'Bảng Giá',
					content: { title: '', description: '' },
					path: 'price'
				}
			])

			await this._priceRepository.bulkCreate(this._dataPrice)

			const data = [
				{ code: 'CTCH', content: this._rentalApartment?.body },
				{ code: 'CTMB', content: this._rentalGround?.body },
				{ code: 'CTPT', content: this._rentalMotel?.body },
				{ code: 'NCT', content: this._rentalHouse?.body },
				{ code: 'TNOG', content: this._roommate?.body }
			]

			this._data['CTCH'] = this._rentalApartment?.body
			this._data['CTMB'] = this._rentalGround?.body
			this._data['CTMB'] = this._rentalGround?.body
			this._data['CTPT'] = this._rentalMotel?.body
			this._data['NCT'] = this._rentalHouse?.body
			this._data['TNOG'] = this._roommate?.body

			data.map(async (dataBody) => {
				for (const item of dataBody.content) {
					// tạo và kiểm tra labelCode
					const labelCode = generateCode(item?.header?.class?.classType).trim()

					const foundLabel = await this._labelRepository.findOne({
						where: { id: labelCode }
					})

					if (!foundLabel) {
						const label = {
							code: labelCode,
							value: item?.header?.class?.classType?.trim()
						}
						try {
							console.log('_labelRepository')
							await this._labelRepository.save({
								id: label?.code,
								value: label?.value,
								isActive: true
							})
						} catch (err) {
							console.log(err)
						}
					}
					// tạo và kiểm tra provinceCode
					const provinceCode = generateCode(item?.header?.address.split(',').slice(-1)[0].trim())

					const foundItem = await this._provinceRepository.findOneBy({
						id: provinceCode
					})
					if (foundItem === null) {
						const province = {
							code: provinceCode,
							value: item?.header?.address?.split(',')?.slice(-1)[0].trim()
						}

						try {
							await this._provinceRepository.save({
								id: provinceCode,
								value: province?.value,
								isActive: true
							})
						} catch (err) {
							console.log(err)
						}
						console.log('_provinceRepository')
					} else {
					}

					const attributeId = uuidv4()
					const userId = uuidv4()
					const overviewId = uuidv4()
					const currentArea = getNumberFromString(item?.header?.attributes?.acreage)
					const currentPrice = getNumberFromString(item?.header?.attributes?.price)

					try {
						console.log('_userRepository')
						await this._userRepository.save({
							id: userId,
							name: item?.contact?.content.find((i) => i.name === 'Liên hệ:')?.content,
							password: hashPassWord('123456'),
							phone: item?.contact?.content.find((i) => i.name === 'Điện thoại:')?.content,
							zalo: item?.contact?.content.find((i) => i.name === 'Zalo')?.content,
							avatar: 'https://phongtro123.com/images/default-user.png',
							isActive: true
						})
					} catch (err) {
						console.log(err)
					}
					try {
						console.log('_attributeRepository')
						await this._attributeRepository.save({
							id: attributeId,
							price: item?.header?.attributes?.price,
							acreage: item?.header?.attributes?.acreage,
							published: item?.header?.attributes?.published,
							hashtag: item?.header?.attributes?.hashtag,
							isActive: true
						})
					} catch (err) {
						console.log(err)
					}
					try {
						console.log('_overviewRepository')
						await this._overviewRepository.save({
							id: overviewId,
							code: item?.overview?.content.find((i) => i.name === 'Mã tin:')?.content,
							area: item?.overview?.content.find((i) => i.name === 'Khu vực')?.content,
							type: item?.overview?.content.find((i) => i.name === 'Loại tin rao:')?.content,
							target: item?.overview?.content.find((i) => i.name === 'Đối tượng thuê:')?.content,
							bonus: item?.overview?.content.find((i) => i.name === 'Gói tin:')?.content,
							created: item?.overview?.content.find((i) => i.name === 'Ngày đăng:')?.content,
							expired: item?.overview?.content.find((i) => i.name === 'Ngày hết hạn:')?.content,
							isActive: true
						})
					} catch (err) {
						console.log(err)
					}
					try {
						console.log('_postRepository')
						await this._postRepository.save({
							id: uuidv4(),
							title: item?.header?.title,
							start: item?.header?.star && +item?.header?.star,
							address: item?.header?.address,
							description: JSON.stringify(item?.mainContent?.content),
							userId: userId,
							labelCode: labelCode,
							provinceCode,
							categoryCode: dataBody.code,
							attributeId: attributeId,
							overviewId: overviewId,
							isActive: true,
							areaCode: this._dataArea.find((area) => area.max > currentArea && area.min <= currentArea)
								?.code,
							priceCode: this._dataPrice.find(
								(area) => area.max > currentPrice && area.min <= currentPrice
							)?.code,
							priceNumber: getNumberFromStringV2(item?.header?.attributes?.price),
							areaNumber: getNumberFromStringV2(item?.header?.attributes?.acreage),
							images: item?.images.join(','),
							postImg: item?.images[0],
							total: item?.images.length
						})
					} catch (err) {
						console.log(err)
					}
				}
			})

			return 'Data inserted successfully'
		} catch (error) {
			console.error('Error inserting data:', error)
			throw new Error('Data insertion failed')
		}
	}
}
