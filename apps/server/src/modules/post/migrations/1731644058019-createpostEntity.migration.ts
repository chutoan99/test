import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class CreatePostEntity1731644058019 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'Posts',
				columns: [
					{
						name: 'id',
						type: 'uuid',
						isPrimary: true,
						default: 'gen_random_uuid()',
					},
					{
						name: 'title',
						type: 'varchar',
						isNullable: true
					},
					{
						name: 'start',
						type: 'int',
						isNullable: true
					},
					{
						name: 'address',
						type: 'varchar',
						isNullable: true
					},
					{
						name: 'description',
						type: 'text',
						isNullable: true
					},
					{
						name: 'priceNumber',
						type: 'float'
					},
					{
						name: 'areaNumber',
						type: 'float'
					},
					{
						name: 'images',
						type: 'text'
					},
					{
						name: 'postImg',
						type: 'varchar',
						isNullable: true
					},
					{
						name: 'total',
						type: 'int'
					},
					{
						name: 'userId',
						type: 'uuid',
						default: 'gen_random_uuid()',
					},
					{
						name: 'overviewId',
						type: 'uuid'
					},
					{
						name: 'attributeId',
						type: 'uuid'
					},
					{
						name: 'categoryCode',
						type: 'varchar'
					},
					{
						name: 'priceCode',
						type: 'varchar'
					},
					{
						name: 'areaCode',
						type: 'varchar'
					},
					{
						name: 'provinceCode',
						type: 'varchar'
					},
					{
						name: 'labelCode',
						type: 'varchar'
					},
					{
						name: 'isActive',
						type: 'boolean',
						isNullable: true
					},
					{
						name: 'createdAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'updatedAt',
						type: 'timestamp',
						default: 'now()'
					},
					{
						name: 'deleteAt',
						type: 'timestamp',
						isNullable: true
					}
				]
			}),
			true
		)

		await queryRunner.createForeignKeys('Posts', [
			// new TableForeignKey({
			// 	columnNames: ['userId'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Users',
			// 	onDelete: 'CASCADE'
			// }),
			// new TableForeignKey({
			// 	columnNames: ['overviewId'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Overviews',
			// 	onDelete: 'CASCADE'
			// }),
			// new TableForeignKey({
			// 	columnNames: ['attributeId'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Attributes',
			// 	onDelete: 'CASCADE'
			// }),
			// new TableForeignKey({
			// 	columnNames: ['categoryCode'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Categories',
			// 	onDelete: 'CASCADE'
			// }),
			// new TableForeignKey({
			// 	columnNames: ['priceCode'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Prices',
			// 	onDelete: 'CASCADE'
			// }),
			// new TableForeignKey({
			// 	columnNames: ['areaCode'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Areas',
			// 	onDelete: 'CASCADE'
			// }),
			// new TableForeignKey({
			// 	columnNames: ['provinceCode'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Provinces',
			// 	onDelete: 'CASCADE'
			// }),
			// new TableForeignKey({
			// 	columnNames: ['labelCode'],
			// 	referencedColumnNames: ['id'],
			// 	referencedTableName: 'Labels',
			// 	onDelete: 'CASCADE'
			// })
		])
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('Posts');
	}
}
