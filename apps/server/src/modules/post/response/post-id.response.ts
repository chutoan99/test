import { BaseResponse } from '@core/models'
import { Field, ObjectType } from '@nestjs/graphql'
import { PostSchema } from '../schema'

@ObjectType()
export class PostIdResponse extends BaseResponse {
	@Field(() => PostSchema)
	response: PostSchema
}
