import { BaseResponse } from '@core/models'
import { ObjectType } from '@nestjs/graphql'

@ObjectType()
export class PostCreateResponse extends BaseResponse {}
