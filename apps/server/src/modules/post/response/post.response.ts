import { BaseResponse } from '@core/models'
import { Field, Int, ObjectType } from '@nestjs/graphql'
import { PostSchema } from '../schema'

@ObjectType()
export class PostResponse extends BaseResponse {
	@Field(() => Int)
	totalPage: number

	@Field(() => Int)
	total: number

	@Field(() => Int)
	pageNumber: number

	@Field(() => Int)
	pageSize: number

	@Field(() => [PostSchema])
	response: PostSchema[]
}
