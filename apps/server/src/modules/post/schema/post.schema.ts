import { AttributeSchema } from '@modules/attribute/schema'
import { OverviewSchema } from '@modules/overview/schema'
import { UserSchema } from '@modules/user/schema'
import { Field, Float, ID, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class PostSchema {
	@Field(() => ID, { nullable: true })
	id: number

	@Field({ nullable: true })
	title: string

	@Field({ nullable: true })
	start: string

	@Field({ nullable: true })
	labelCode: string

	@Field({ nullable: true })
	address: string

	@Field({ nullable: true })
	attributesId: string

	@Field({ nullable: true })
	categoryCode: string

	@Field({ nullable: true })
	priceCode: string

	@Field({ nullable: true })
	areaCode: string

	@Field({ nullable: true })
	provinceCode: string

	@Field({ nullable: true })
	description: string

	@Field({ nullable: true })
	userId: string

	@Field({ nullable: true })
	overviewId: string

	@Field({ nullable: true })
	imagesId: string

	@Field(() => Float, { nullable: true })
	priceNumber: number

	@Field(() => Float, { nullable: true })
	areaNumber: number

	@Field({ nullable: true })
	images: string

	@Field({ nullable: true })
	postImg: string

	@Field()
	total: number

	@Field(() => Date, { nullable: true })
	createdAt: Date

	@Field(() => Date, { nullable: true })
	updatedAt: Date

	@Field(() => UserSchema)
	user: UserSchema

	@Field(() => AttributeSchema)
	attributes: AttributeSchema

	@Field(() => OverviewSchema)
	overviews: OverviewSchema
}
