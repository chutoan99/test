import { AttributeEntity } from '@modules/attribute/entity'
import { LabelEntity } from '@modules/label/entity'
import { OverviewEntity } from '@modules/overview/entity'
import { ProvinceEntity } from '@modules/province/entity'
import { InjectRepository } from '@nestjs/typeorm'
import generateCode from '@utils/generateCode'
import generaDate from '@utils/generateDate'
import { Repository, UpdateResult } from 'typeorm'
import { v4 as uuidv4 } from 'uuid'
import { PostCreateDto, PostUpdateDto } from '../dto'
import { PostEntity } from '../entity'
import moment from 'moment'

export type IPostRepository = {
	getAllPost(where: any, limit: number, offset: number): Promise<[PostEntity[], number]>
	getPostId(id: string): Promise<{
		err: number
		msg: string
		response: PostEntity
	}>
	createPost(input: PostCreateDto): Promise<{
		err: number
		msg: string
	}>
	deletePostId(id: string): Promise<boolean>
	updatePostId(id: string, input: PostUpdateDto): Promise<boolean>
}

export class PostRepository implements IPostRepository {
	constructor(
		@InjectRepository(ProvinceEntity)
		private readonly _provinceRepository: Repository<ProvinceEntity>,

		@InjectRepository(AttributeEntity)
		private readonly _attributeRepository: Repository<AttributeEntity>,

		@InjectRepository(LabelEntity)
		private readonly _labelRepository: Repository<LabelEntity>,

		@InjectRepository(PostEntity)
		private readonly _postRepository: Repository<PostEntity>,

		@InjectRepository(OverviewEntity)
		private readonly _overviewRepository: Repository<OverviewEntity>
	) {}

	public async getAllPost(where: any, limit: number, offset: number) {
		return await this._postRepository.findAndCount({
			where: where,
			take: limit,
			skip: offset
		})
	}

	public async getPostId(id: string) {
		const response = await this._postRepository.findOne({
			where: {
				id
			}
		})
		return {
			err: response ? 0 : 1,
			msg: response ? 'OK' : 'Failed to get postId',
			response
		}
	}

	public async createPost(input: PostCreateDto) {
		try {
			const userid = input.userid
			const address = input.address
			const areaCode = input.areaCode
			const areaNumber = input.areaNumber
			const categoryCode = input.categoryCode
			const description = input.description
			const images = input.images
			const label = input.label
			const priceCode = input.priceCode
			const priceNumber = input.priceNumber
			const province = input.province
			const target = input.target
			const title = input.title
			const type = input.type
			const start = input.start

			const attributeId = uuidv4()
			const overviewId = uuidv4()
			const hashtag = `${Math.floor(Math.random() * Math.pow(10, 6))}`
			const currentDate = generaDate()
			const labelCode = generateCode(label)
			const priceUnit = +priceNumber < 1 ? 'đồng/tháng' : 'triệu/tháng'
			const price = `${+priceNumber * (priceUnit === 'đồng/tháng' ? 1000000 : 1)} ${priceUnit}`

			// Create post
			const post = this._postRepository.create({
				id: uuidv4(),
				attributeId,
				labelCode,
				overviewId,
				title: title || null,
				start: start || 0,
				address: address || null,
				description: JSON.stringify(description) || null,
				userId: userid,
				categoryCode,
				provinceCode: province.includes('Thành phố')
					? generateCode(province.replace('Thành phố', ''))
					: generateCode(province.replace('Tỉnh', '')),
				images: images.join(','),
				postImg: images[0],
				total: images.length,
				areaCode,
				priceCode,
				priceNumber,
				areaNumber,
				isActive: true
			})
			await this._postRepository.save(post)

			// Create Attribute
			const attribute = this._attributeRepository.create({
				id: attributeId,
				price,
				acreage: `${areaNumber} m2`,
				published: moment(new Date()).format('DD/MM/YYYY'),
				hashtag: `#${hashtag}`
			})
			await this._attributeRepository.save(attribute)

			// Create Overview
			await this._overviewRepository.save({
				id: overviewId,
				code: `#${hashtag}`,
				area: label,
				type: type,
				target: target,
				bonus: 'Tin thường',
				created: currentDate.today,
				expired: currentDate.expireDay
			})

			// Create or find Province
			const provinceRecord = await this._provinceRepository.findOne({
				where: [{ value: province.replace('Thành phố', '') }, { value: province.replace('Tỉnh', '') }]
			})
			let provinceEntity: ProvinceEntity
			if (provinceRecord) {
				provinceEntity = provinceRecord
			} else {
				provinceEntity = this._provinceRepository.create({
					id: province.includes('Thành phố')
						? generateCode(province.replace('Thành phố', ''))
						: generateCode(province.replace('Tỉnh', '')),
					value: province.includes('Thành phố')
						? province.replace('Thành phố', '')
						: province.replace('Tỉnh', '')
				})
				await this._provinceRepository.save(provinceEntity)
			}

			// Create or find Label
			const labelRecord = await this._labelRepository.findOne({
				where: { id: labelCode }
			})
			let labelEntity: LabelEntity
			if (labelRecord) {
				labelEntity = labelRecord
			} else {
				labelEntity = this._labelRepository.create({
					id: labelCode,
					value: label
				})
				await this._labelRepository.save(labelEntity)
			}

			return {
				err: 0,
				msg: 'create post success'
			}
		} catch (error) {
			throw error
		}
	}

	public async updatePostId(id: string, input: PostUpdateDto): Promise<boolean> {
		const response: UpdateResult = await this._postRepository.update(id, {
			title: input.title,
			address: input.address
		})

		return response.affected > 0
	}

	public async deletePostId(id: string): Promise<boolean> {
		const response: UpdateResult = await this._postRepository.update(id, {
			isActive: false
		})

		return response.affected > 0
	}
}
