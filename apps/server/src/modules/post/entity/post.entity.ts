import { BaseEntityModel } from '@core/models'
import { AreaEntity } from '@modules/area/entitiy'
import { AttributeEntity } from '@modules/attribute/entity'
import { CategoryEntity } from '@modules/category/entity'
import { LabelEntity } from '@modules/label/entity'
import { OverviewEntity } from '@modules/overview/entity'
import { PriceEntity } from '@modules/price/entity'
import { ProvinceEntity } from '@modules/province/entity'
import { UserEntity } from '@modules/user/entity'
import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm'
import { v4 as uuidv4 } from 'uuid'

@Entity('Posts')
export class PostEntity extends BaseEntityModel {
	@PrimaryColumn('uuid', { default: uuidv4() })
	id: string

	@Column()
	title: string | null

	@Column({ nullable: true })
	start: number | null

	@Column()
	address: string | null

	@Column('text')
	description: string | null

	@Column('float')
	priceNumber: number

	@Column('float')
	areaNumber: number

	@Column('text')
	images: string

	@Column({ nullable: true })
	postImg: string

	@Column()
	total: number

	@Column('uuid', { default: uuidv4() })
	userId: string

	@ManyToOne(() => UserEntity, (user: UserEntity) => user.posts)
	user: UserEntity

	@Column()
	overviewId: string

	@OneToOne(() => OverviewEntity, { cascade: true })
	@JoinColumn({ name: 'overviewId', referencedColumnName: 'id' })
	overview: OverviewEntity

	@Column()
	attributeId: string

	@OneToOne(() => AttributeEntity, { cascade: true })
	@JoinColumn({ name: 'attributeId', referencedColumnName: 'id' })
	attribute: AttributeEntity

	@Column()
	categoryCode: string

	@ManyToOne(() => CategoryEntity, (category: CategoryEntity) => category.posts)
	@JoinColumn({ name: 'categoryCode', referencedColumnName: 'id' })
	category: CategoryEntity

	@Column()
	priceCode: string

	@ManyToOne(() => PriceEntity, (price: PriceEntity) => price.posts)
	@JoinColumn({ name: 'priceCode', referencedColumnName: 'id' })
	price: PriceEntity

	@Column()
	areaCode: string

	@ManyToOne(() => AreaEntity, (area: AreaEntity) => area.posts)
	@JoinColumn({ name: 'areaCode', referencedColumnName: 'id' })
	area: AreaEntity

	@Column()
	provinceCode: string

	@ManyToOne(() => ProvinceEntity, (province: ProvinceEntity) => province.posts)
	@JoinColumn({ name: 'provinceCode', referencedColumnName: 'id' })
	province: ProvinceEntity

	@Column()
	labelCode: string

	@ManyToOne(() => LabelEntity, (label: LabelEntity) => label.posts)
	@JoinColumn({ name: 'labelCode', referencedColumnName: 'id' })
	label: LabelEntity
}
