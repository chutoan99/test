import { AttributeEntity } from '@modules/attribute/entity'
import { AttributeSchema } from '@modules/attribute/schema'
import { OverviewEntity } from '@modules/overview/entity'
import { OverviewSchema } from '@modules/overview/schema'
import { UserEntity } from '@modules/user/entity'
import { UserSchema } from '@modules/user/schema'
import { Inject } from '@nestjs/common'
import { Args, ID, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { GetPostDto, PostCreateDto, PostUpdateDto } from '../dto'
import { PostCreateResponse, PostDeleteResponse, PostIdResponse, PostResponse, PostUpdateResponse } from '../response'
import { PostSchema } from '../schema'
import { IPostService } from '../service'
import { IOverviewRepository } from '@modules/overview/repository'

@Resolver(() => PostSchema) // Specify the object type as Post
export class PostResolver {
	constructor(
		@Inject('IPostService')
		private readonly _postService: IPostService,

		@Inject('IOverviewRepository')
		private readonly _overviewRepository: IOverviewRepository,

		@InjectRepository(UserEntity)
		private readonly _userRepository: Repository<UserEntity>,

		@InjectRepository(AttributeEntity)
		private readonly _attributeRepository: Repository<AttributeEntity>
	) {}

	@Query(() => PostResponse)
	public post(
		@Args('input', {
			type: () => GetPostDto
		})
		input: GetPostDto
	) {
		return this._postService.getAllPost(input)
	}

	@Query(() => PostIdResponse)
	public postId(
		@Args('postId', {
			type: () => ID
		})
		id: string
	) {
		return this._postService.getPostId(id)
	}

	@Mutation(() => PostCreateResponse)
	public createPost(
		@Args('input', {
			type: () => PostCreateDto
		})
		input: PostCreateDto
	) {
		return this._postService.createPost(input)
	}

	@Mutation(() => PostUpdateResponse)
	public updatePost(
		@Args('postId', {
			type: () => ID
		})
		id: string,
		@Args('input', {
			type: () => PostUpdateDto
		})
		input: PostUpdateDto
	) {
		return this._postService.updatePostId(id, input)
	}

	@Mutation(() => PostDeleteResponse)
	public deletePost(
		@Args('postId', {
			type: () => ID
		})
		id: string
	) {
		return this._postService.deletePostId(id)
	}

	@ResolveField(() => UserSchema)
	public user(@Parent() post: PostSchema) {
		return this._userRepository.findOne({
			where: { id: post.userId }
		})
	}

	@ResolveField(() => AttributeSchema)
	public attributes(@Parent() post: PostSchema) {
		return this._attributeRepository.findOne({
			where: { id: post.attributesId }
		})
	}

	@ResolveField(() => OverviewSchema)
	public overviews(@Parent() post: PostSchema): Promise<OverviewEntity> {
		return this._overviewRepository.findOne(post.overviewId)
	}
}
