import { AttributeEntity } from '@modules/attribute/entity'
import { LabelEntity } from '@modules/label/entity'
import { OverviewEntity } from '@modules/overview/entity'
import { OverviewRepository } from '@modules/overview/repository'
import { ProvinceEntity } from '@modules/province/entity'
import { UserEntity } from '@modules/user/entity'
import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PostEntity } from './entity'
import { PostResolver } from './resolver'
import { PostService } from './service'

@Module({
	imports: [
		TypeOrmModule.forFeature([PostEntity, ProvinceEntity, AttributeEntity, LabelEntity, OverviewEntity, UserEntity])
	],
	providers: [
		PostResolver,
		{
			provide: 'IPostService',
			useClass: PostService
		},
		{
			provide: 'IOverviewRepository',
			useClass: OverviewRepository
		}
	]
})
export class PostModule {}
