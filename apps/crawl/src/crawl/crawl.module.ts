import { Module } from '@nestjs/common'
import { PuppeteerModule } from 'nest-puppeteer'
import { CrawlController } from './crawl.controller'
import { CrawlService } from './crawl.service'

@Module({
	imports: [
		PuppeteerModule.forRoot(
			{ pipe: true }, // optional, any Puppeteer launch options here or leave empty for good defaults */,
			'BrowserInstanceName' // optional, can be useful for using Chrome and Firefox in the same project
		)
	],
	providers: [CrawlService],
	controllers: [CrawlController]
})
export class CrawlModule {}
