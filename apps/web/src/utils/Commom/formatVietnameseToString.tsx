export const formatVietnameseToString = (keyWord: string) => {
	return keyWord
		.toLowerCase()
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, '')
		.split(' ')
		.join('-')
}
