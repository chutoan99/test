import { DetailLayout } from '@layouts/detail'
import { PostDetailComponent } from '@modules/postDetail'


export default function IndexDetailPage() {
	return <PostDetailComponent />
}
IndexDetailPage.Layout = DetailLayout
