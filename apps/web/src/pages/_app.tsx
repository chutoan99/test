import '../styles/globals.css'
import '../styles/style.css'
import '../styles/system.css'

import { AppProps } from 'next/app'
import React, { ReactNode } from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'

type LayoutProps = {
	children: ReactNode
}

type NextComponentWithLayout = {
	Layout?: React.ComponentType
}

function MyApp({ Component, pageProps }: AppProps) {
	const [queryClient] = React.useState(() => new QueryClient())
	const LayoutApp = (Component as NextComponentWithLayout)?.Layout || EmptyLayout

	return (
		<QueryClientProvider client={queryClient}>
			<LayoutApp>
				<Component {...pageProps} />
			</LayoutApp>
			<ReactQueryDevtools />
		</QueryClientProvider>
	)
}

const EmptyLayout = ({ children }: LayoutProps) => <>{children}</>
export default MyApp
