import CategoryComponent from '@modules/category/category.component'
import HeaderComponent from '@modules/header/header.component'

type AuthLayoutProps = {
	children: any
}

export default function AuthLayout({ children }: AuthLayoutProps) {
	return (
		<div id='webpage'>
			<HeaderComponent />
			<CategoryComponent />
			{children}
		</div>
	)
}
