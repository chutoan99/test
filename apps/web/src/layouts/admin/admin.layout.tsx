import useAuth from "@modules/auth/hooks/useAuth.hook"
import { AsideComponent, NavComponent } from "@modules/shared/admin"
import { useQueryUserId } from "@modules/user/hooks"
import { useEffect } from "react"

type AdminLayoutProps = {
	children: any
}

export default function AdminLayout({ children }: AdminLayoutProps) {
	const { dataUser } = useAuth()
	const { data, isLoading } = useQueryUserId(dataUser?.id || '')

	return (
		<div className='desktop dashboard loaded ready'>
			<div id='webpage' style={{ position: 'relative' }}>
				<NavComponent />
				<div className='container-fluid' style={{ position: 'absolute', top: '45px' }}>
					{!isLoading ? (
						<div className='row'>
							<div className='d-flex'>
								{data && <AsideComponent dataUser={data} />}

								{children}
							</div>
						</div>
					) : (
						<span className='loader'></span>
					)}
				</div>
			</div>
		</div>
	)
}
