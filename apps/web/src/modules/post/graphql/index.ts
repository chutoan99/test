export const createPostFilePath = require('./create_post.graphql')
export const updatePostFilePath = require('./update_post.graphql')
export const deletePostFilePath = require('./delete_post.graphql')
export const postFilePath = require('./post.graphql')
