import Swal from 'sweetalert2'
import { GraphQLClient } from 'graphql-request'
import { categoryFilePath } from '../graphql'
import { CategoryResponse } from 'graphql.interface'

const graphQLClient = new GraphQLClient(process.env.NEXT_PUBLIC_API_URL_DEV!)
export const queryCategories = async () => {
	try {
		const response: { category: CategoryResponse } = await graphQLClient.request(categoryFilePath)
		if (response.category.err === 0) {
			return response.category.response
		} else {
			Swal.fire('Oop !', response.category.msg, 'error')
			return
		}
	} catch (error) {
		throw new Error('Failed to fetch category')
	}
}
