import Swal from 'sweetalert2'
import { GraphQLClient } from 'graphql-request'
import { updateUserFilePath, userIdFilePath, userPostsFilePath } from '../graphql'
import { UpdateUserDto, UserIdResponse, UserResponse, UserSchema, UserUpdateResponse } from 'graphql.interface'

const graphQLClient = new GraphQLClient(process.env.NEXT_PUBLIC_API_URL_DEV!)
export const queryUserId = async (userId: string) => {
	try {
		if (userId === '') return

		const response: { userId: UserIdResponse } = await graphQLClient.request(userIdFilePath, {
			userId: userId
		})
		if (response.userId && response.userId.err === 0) {
			return response.userId.response
		} else {
			Swal.fire('Oop !', response.userId.msg, 'error')
			return
		}
	} catch (error) {
		throw new Error('Failed to fetch current user')
	}
}

export const queryPostsOfUser = async (userId: string) => {
	try {
		if (userId === '') return

		const response: { userId: UserIdResponse } = await graphQLClient.request(userPostsFilePath, {
			userId: userId
		})
		console.log(response,"responseresponse")
		if (response.userId.err === 0) {
			return response.userId.response 
		} else {
			Swal.fire('Oop !', response.userId.msg, 'error')
			return
		}
	} catch (error) {
		throw new Error('Failed to fetch all post of user')
	}
}

export const mutationUpdateUser = async (userId: string, payloadUpdateUser: UpdateUserDto) => {
	try {
		if (userId === '') return

		const response: { userId: UserUpdateResponse } = await graphQLClient.request(updateUserFilePath, {
			userId: userId,
			input: { ...payloadUpdateUser }
		})
		if (response.userId.err === 0) {
			Swal.fire('Oop !', response.userId.msg, 'success')
		} else {
			Swal.fire('Oop !', response.userId.msg, 'error')
			return
		}
	} catch (error) {
		throw new Error('Failed to fetch update user')
	}
}
