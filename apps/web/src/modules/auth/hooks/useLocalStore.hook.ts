import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'

const useLocalStore = () => {
	const router = useRouter()

	const [data, setData] = useState({
		token: null,
		isLogin: false,
		id: ''
	})

	useEffect(() => {
		if (typeof window !== 'undefined' && window.localStorage) {
			const dataFromLocalStorage = localStorage.getItem('token')
			if (!dataFromLocalStorage) {
				if (!['/register', '/login'].includes(router.pathname)) {
					console.log('csc')
					router.push('/login')
				}
			} else {
				const parsedData = JSON.parse(dataFromLocalStorage)
				setData(parsedData)
			}
		}
	}, [router])

	return {
		isLogin: data.isLogin,
		userId: data.id
	}
}

export default useLocalStore
