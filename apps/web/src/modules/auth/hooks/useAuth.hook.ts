import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useQueryUserId } from '../../user/hooks/user.hook'
import { UserSchema } from 'graphql.interface'

const useAuth = () => {
	const router = useRouter()

	// Dữ liệu từ localStorage
	const [dataLocalStore, setDataLocalStore] = useState({
		token: null,
		isLogin: false,
		id: ''
	})

	// Lấy token từ localStorage và cập nhật dữ liệu
	useEffect(() => {
		if (typeof window !== 'undefined' && window.localStorage) {
			const dataFromLocalStorage = localStorage.getItem('token')
			if (!dataFromLocalStorage) {
				if (!['/register', '/login'].includes(router.pathname)) {
					router.push('/login')
				}
			} else {
				const parsedData = JSON.parse(dataFromLocalStorage)
				setDataLocalStore(parsedData)
			}
		}
	}, [router])
	// Gọi hook để lấy thông tin người dùng
	const { data, isLoading } = useQueryUserId(dataLocalStore.id)

	// Xử lý khi có dữ liệu người dùng
	const [dataUser, setDataUser] = useState<UserSchema | undefined>()
	useEffect(() => {
		if (data) {
			setDataUser(data)
		}
	}, [data, isLoading])

	return { dataUser, isLogin: dataLocalStore.isLogin, isLoading }
}

export default useAuth
