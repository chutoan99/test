import { LoginDto, LoginSchema } from 'graphql.interface'
import { GraphQLClient } from 'graphql-request'
import { loginFilePath } from '../graphql'

export interface LoginResponse {
	login: LoginSchema
}

export const LoginService = async (payload: LoginDto) => {
	try {
		const graphQLClient = new GraphQLClient(process.env.NEXT_PUBLIC_API_URL_DEV!)
		const response: LoginResponse = await graphQLClient.request<any>(loginFilePath, {
			input: { ...payload }
		})

		if (response.login.err === 0) {
			return response
		}
	} catch (error) {
		console.error('Error:', error)
	}
}
