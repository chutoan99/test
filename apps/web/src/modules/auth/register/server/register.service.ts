import { RegisterDto, RegisterSchema } from 'graphql.interface'
import { GraphQLClient } from 'graphql-request'
import { registerFilePath } from '../graphql'

export interface RegisterResponse {
	register: RegisterSchema
}

export const RegisterService = async (payload: RegisterDto) => {
	try {
		const graphQLClient = new GraphQLClient(process.env.NEXT_PUBLIC_API_URL_DEV!)
		const response: RegisterResponse = await graphQLClient.request<any>(registerFilePath, {
			input: { ...payload }
		})

		if (response.register.err === 0) {
			return response
		}
	} catch (error) {
		console.error('Error:', error)
	}
}
