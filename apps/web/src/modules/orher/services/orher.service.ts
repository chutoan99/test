import axios from 'axios'
import Swal from 'sweetalert2'


export type ProvinceModel = {
	code: string
	name: string
}

export type DistrictModel = {
	code: string
	name: string
	province: string
}

export type WardModel = {
	code: string
	name: string
	district: string
	province: string
}


export const GetALLProvince = async () => {
	try {
		const response = await axios({
			method: 'get',
			url: 'https://api.mysupership.vn/v1/partner/areas/province'
		})
		if (response.status === 200) {
			return response.data.results
		} else {
			return []
		}
	} catch (error) {
		console.log(error, 'error')
	}
}

export const GetAllDistrictWithProvinceCode = async (provinceCode: any) => {
	try {
		const response = await axios({
			method: 'get',
			url: `https://api.mysupership.vn/v1/partner/areas/district?province=${provinceCode}`
		})
		if (response.status === 200) {
			return response.data.results
		} else {
			return []
		}
	} catch (error: any) {
		Swal.fire('Oop !', error.toString(), 'error')
	}
}

export const GetAllWardWithDistrictCode = async (districtCode: any) => {
	try {
		const response = await axios({
			method: 'get',
			url: `https://api.mysupership.vn/v1/partner/areas/commune?district=${districtCode}`
		})
		if (response.status === 200) {
			return response.data.results
		} else {
			return []
		}
	} catch (error: any) {
		Swal.fire('Oop !', error.toString(), 'error')
	}
}

export const apiUploadImages = async (fileImages: File[]) => {
	try {
		const uploadPromises = fileImages.map(async (file) => {
			const formData = new FormData()
			formData.append('file', file)
			formData.append('upload_preset', process.env.NEXT_PUBLIC_UPLOAD_ASSETS_NAME!)

			try {
				const response = await axios({
					method: 'post',
					url: `https://api.cloudinary.com/v1_1/${process.env.NEXT_PUBLIC_CLOUDINARY_NAME}/image/upload/`,
					data: formData
				})

				if (response.status === 200) {
					return response?.data?.url
				}
			} catch (error) {
				console.error('Error uploading image:', error)
				throw error // Re-throw the error to be caught by Promise.all
			}
		})

		const images = await Promise.all(uploadPromises)

		if (images.every((url) => url)) {
			return images
		} else {
			Swal.fire('Error!', 'Some images failed to upload.', 'error')
			return null
		}
	} catch (error: any) {
		Swal.fire('Oop !', error.toString(), 'error')
	}
}
