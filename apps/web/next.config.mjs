import { fileURLToPath } from 'url';
import { dirname, join } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export default {
  reactStrictMode: true,
  swcMinify: true,
  experimental: {
    appDir: false,
    // Remove invalid 'appDir' key
  },
  sassOptions: {
    includePaths: [join(__dirname, 'style')],
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(graphql|gql)$/,
      exclude: /node_modules/,
      use: ['graphql-tag/loader'],
    });
    return config;
  },
};
